package org.mpierce.ktor.multipart

import io.ktor.application.call
import io.ktor.content.PartData
import io.ktor.content.readAllParts
import io.ktor.http.ContentDisposition
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.headersOf
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import io.ktor.util.hex
import org.junit.jupiter.api.Test
import java.util.concurrent.ThreadLocalRandom
import kotlin.test.assertEquals

class MultipartTest {
    @Test
    internal fun multipartFileUpload() {
        val bytes = ByteArray(3)
        ThreadLocalRandom.current().nextBytes(bytes)

        withTestApplication({
            routing {
                post("/upload") {
                    val parts = call.receiveMultipart().readAllParts()

                    val titlePart = parts.first { it.name == "title" } as PartData.FormItem
                    val filePart = parts.first { it.name == "file" } as PartData.FileItem

                    call.respond(HttpStatusCode.OK, "title ${titlePart.value} hex ${hex(
                            filePart.streamProvider().readAllBytes())}")
                }
            }
        }) {
            with(handleRequest(HttpMethod.Post, "/upload") {
                val boundary = "***bbb***"

                addHeader(HttpHeaders.ContentType,
                        ContentType.MultiPart.FormData.withParameter("boundary", boundary).toString())
                setBody(boundary, listOf(
                        PartData.FormItem("title123", { }, headersOf(
                                HttpHeaders.ContentDisposition,
                                ContentDisposition.Inline
                                        .withParameter(ContentDisposition.Parameters.Name, "title")
                                        .toString()
                        )),
                        PartData.FileItem({ bytes.inputStream() }, {}, headersOf(
                                HttpHeaders.ContentDisposition,
                                ContentDisposition.File
                                        .withParameter(ContentDisposition.Parameters.Name, "file")
                                        .withParameter(ContentDisposition.Parameters.FileName, "file.txt")
                                        .toString()
                        ))
                ))
            }) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("title title123 hex ${hex(bytes)}", response.content!!)
            }
        }
    }
}
